const signup = document.getElementById("signup");
const app =  document.getElementById("app");

const state = {
    isAuthorized: false,
    key: "b2e7926c2fe12f7560476f3d91cc9adf",
    cityId: 484907,
    data: {
        name: '',
        temp: 0,
        feelsLike: 0,
        windSpeed: 0,
        weather: '',
        humidity: 0,
        pressure: 0,
        weatherIcon: '',
        conditionName: '',
        weatherBgGradient: 'hcl_d',
        weatherBg: 'clouds_hi',
        isDay: true
    },
}

const render = () => {
    if (!state.isAuthorized) {
        signup.classList.remove("noDisplay")
        app.classList.add("noDisplay")
        const registrationForm = document.getElementById("registrationForm");
        registrationForm.onsubmit = registration;
    } else {
        requestNowForecast().then(() => {
            app.classList.remove("noDisplay")
            signup.classList.add("noDisplay")
        });
    }
};

const registration = () => {
    state.isAuthorized = true;
    state.cityId = document.getElementById("city").value;
    render();
    return false;
}

const isDay = (currentTime, sunsetTime, sunriseTime) => {
    if (currentTime < sunsetTime && currentTime > sunriseTime) {
        return true;
    } else return !((currentTime > sunsetTime && currentTime < sunriseTime) || (currentTime > sunsetTime && currentTime > sunriseTime) || (currentTime < sunsetTime && currentTime < sunriseTime));
}

const getConditionName = (id) => {
    // Осадки

    if (id >= 200 && id <= 202) return 'Дождь с грозой';
    if (id >= 230 && id <= 232) return 'Дождь с грозой';
    if (id >= 210 && id <= 221) return 'Гроза';

    if(id >= 300 && id <= 321) return 'Морось';
    if(id === 500 || id === 520) return 'Небольшой дождь';
    if(id === 501 || id === 521) return 'Дождь';
    if((id >= 502 && id <= 504) || (id >= 522 && id <= 531)) return 'Сильный дождь';
    if(id === 511) return 'Замерзающий дождь';
    if(id === 600 || id === 620) return 'Небольшой снег';
    if(id === 601 || id === 621) return 'Снег';
    if(id === 602 || id === 622) return 'Снегопад';
    if(id >= 611 && id <= 616) return 'Дождь со снегом';

    // Туман или дымка

    if(id >= 701 && id <= 781) return 'Туман, дымка';

    // Ясно, облака

    if(id === 800) return "Ясно";
    if(id === 801) return "Малооблачно";
    if(id === 802) return "Переменная облачность";
    if(id === 803) return "Облачно";
    if(id === 804) return "Пасмурно";
}

const getWeatherIcon = (id, isDay) => {
    if(isDay) {
        // Без осадков

        if (id === 800) return 'clear_d';
        if (id === 801) return 'lcloudy_d';
        if (id === 802) return 'lcloudy_d';
        if (id === 803) return 'mcloudy_d';
        if (id === 804) return 'hicloudy';

        // Гроза

        if (id >= 200 && id <= 202) return 'ts';
        if (id >= 230 && id <= 232) return 'ts';

        if (id >= 210 && id <= 221) return 'ts_dry';

        // Морось

        if (id >= 300 && id <= 321) return 'rain_light';

        // Дождь

        if (id === 500 || id === 520) return 'rain_light';
        if (id === 501 || id === 521 || id === 511) return 'rain_med';
        if ((id >= 502 && id <= 504) || (id >= 522 && id <= 531)) return 'rain_hi';

        // Снег, мокрый снег

        if (id === 600 || id === 620) return 'snow_light';
        if (id === 601 || id === 621) return 'snow_med';
        if (id === 602 || id === 622) return 'snow_hi';
        if (id >= 611 && id <= 616) return 'sleet';

        // Туман, дымка

        if (id >= 701 && id <= 781) return 'fog';
    } else {
        // Без осадков

        if (id === 800) return 'clear_n';
        if (id === 801) return 'lcloudy_n';
        if (id === 802) return 'lcloudy_n';
        if (id === 803) return 'mcloudy_n';
        if (id === 804) return 'hicloudy';

        // Гроза

        if (id >= 200 && id <= 202) return 'ts';
        if (id >= 230 && id <= 232) return 'ts';

        if (id >= 210 && id <= 221) return 'ts_dry';

        // Морось

        if (id >= 300 && id <= 321) return 'rain_light';

        // Дождь

        if (id === 500 || id === 520) return 'rain_light';
        if (id === 501 || id === 521 || id === 511) return 'rain_med';
        if ((id >= 502 && id <= 504) || (id >= 522 && id <= 531)) return 'rain_hi';

        // Снег, мокрый снег

        if (id === 600 || id === 620) return 'snow_light';
        if (id === 601 || id === 621) return 'snow_med';
        if (id === 602 || id === 622) return 'snow_hi';
        if (id >= 611 && id <= 616) return 'sleet';

        // Туман, дымка

        if (id >= 701 && id <= 781) return 'fog';
    }
}

const getWeatherBgGradient = (id, isDay) => {
    if(isDay) {
        if((id >= 200 && id <= 622) || id === 804) return 'hcl_d';
        if(id >= 701 && id <= 781) return 'cl_d';
        if(id === 800) return 'clear_d';
        if(id >= 801 && id <= 803) return 'cl_d';
    } else {
        if((id >= 200 && id <= 622) || id === 804) return 'hcl_n';
        if(id >= 701 && id <= 781) return 'cl_n';
        if(id === 800) return 'clear_n';
        if(id >= 801 && id <= 803) return 'cl_n';
    }
}

const getWeatherBg = (id) => {
    if (id === 800) return null;
    if (id === 801) return 'clouds_light';
    if (id === 802) return 'clouds_light';
    if (id === 803) return 'clouds_med';
    if (id === 804) return 'clouds_hi';

    if (id >= 200 && id <= 202) return 'rain_hi';
    if (id >= 230 && id <= 232) return 'rain_hi';
    if (id >= 210 && id <= 221) return 'rain_hi';

    if(id >= 300 && id <= 321) return 'rain_light';
    if(id === 500 || id === 520) return 'rain_light';
    if(id === 501 || id === 521) return 'rain_med';
    if((id >= 502 && id <= 504) || (id >= 522 && id <= 531)) return 'rain_hi';
    if(id === 511) return 'rain_med';
    if(id === 600 || id === 620) return 'snow_light';
    if(id === 601 || id === 621) return 'snow_med';
    if(id === 602 || id === 622) return 'snow_hi';
    if(id >= 611 && id <= 616) return 'sleet';

    // Туман или дымка

    if(id >= 701 && id <= 781) return 'clouds_light';
}

const requestNowForecast = async () => {
    try {
        const request = await fetch(`https://api.openweathermap.org/data/2.5/weather?id=${state.cityId}&units=metric&lang=ru&appid=${state.key}`);
        if(request.ok) {
            const json = await request.json();
            state.data.name = json.name;
            document.getElementById("app_city").innerHTML = state.data.name;
            state.data.temp = Math.floor(json.main.temp);
            document.getElementById("app_temp").innerHTML =  `${state.data.temp}°C`;
            state.data.feelsLike = Math.floor(json.main.feels_like);
            document.getElementById("app_feelsLike").innerHTML = `Ощущается как ${state.data.feelsLike }°C`;
            state.data.windSpeed = json.wind.speed;
            document.getElementById("app_windSpeed").innerHTML = `${state.data.windSpeed } м/с`;
            state.data.weather = json.weather[0].main;
            state.data.humidity = json.main.humidity;
            document.getElementById("app_humidity").innerHTML = `${state.data.humidity } %`;
            state.data.pressure = Math.floor(((json.main.pressure ?? 0) / 1.33));
            document.getElementById("app_pressure").innerHTML = `${state.data.pressure } мм рт.ст.`;
            state.data.conditionName = getConditionName(json.weather[0].id);
            document.getElementById("app_weather").innerHTML = `${state.data.conditionName}`;
            state.data.isDay = isDay(json.dt, json.sys.sunset, json.sys.sunrise);
            state.data.weatherIcon = getWeatherIcon(json.weather[0].id, state.data.isDay);
            document.getElementById("weather-icon").src = `img/IconsWeather/${state.data.weatherIcon}.svg`;
            state.data.weatherBgGradient = getWeatherBgGradient(json.weather[0].id, state.data.isDay);
            document.getElementById("weather-bg-gradient").className = `weather-bg-gradient bg-gradient-${state.data.weatherBgGradient}`;
            state.data.weatherBg = getWeatherBg(json.weather[0].id);
            document.getElementById("weather-bg").className = (state.data.weatherBg == null ? "weather-bg" : `weather-bg bg-${state.data.weatherBg}`);
        }
    } catch {
        state.data.name = "Нет данных";
    }
}

render();




